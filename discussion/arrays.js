
// BBASIC ARRAY STRUCTURE
let bootcamp = [
	'Learn HTML',
	'Use CSS',
	'Understand JS',
	'Maintain MongoDB',
	'Create Components using React',
];

// ACCESS ELEMENTS INSIDE AN ARRAY

console.log(bootcamp);
// container/array[IndexNumber];

console.log(bootcamp[0]); //Learn HTML
console.log(bootcamp[4]); //Maintain MongoDB
console.log(bootcamp[6]); //undefined if exceeded


console.log(bootcamp.length);

// if (bootcamp.length > 5) {
// 	console.log('This is how long the array is. Do not exceed')
// }



console.log(bootcamp.length - 1);

console.log(bootcamp[bootcamp.length - 1]);

console.log('BATCH 145');


// ARRAY MANIPULATORS

//1. Mutators

let bootcampTasks = [];
	// push() - to add element at the end of array
	bootcampTasks.push('Learn Javascript');
	bootcampTasks.push('Building a Server using Node');
	bootcampTasks.push('Utilizing Express to build a Server');

	// pop() - remove the last element from array
	let elementRemovedUsingPop =	bootcampTasks.pop();
	console.log(elementRemovedUsingPop);


	// unshift() - adds one or mmore element at 'front' of the array.
	bootcampTasks.unshift(
		'Understand the concept of REST API',
		'How to use Postman',
		'Learn how to use MongoDB'
	);

	// shift() - removes first element at the front of the array. also takes teh removed elemet and place in nnew variable
	let akoTanggalDahilKayShift =bootcampTasks.shift();
	console.log(akoTanggalDahilKayShift);

	// splice() - 
		// syntax: arrayName.splice(number only [StartingPosition], [# of elements to remove], OPTIONAL [Elements to be added]);

		//identify where to begin extraction 
		// remove all elements inside the current array.
		bootcampTasks.splice(0, bootcampTasks.length -1, 'Learn Wireframing', 'Learn React');






//console.log(bootcampTasks);


let library = ['Pride and Prejudice', 'The Alchemist', 'Diary of a Pulubi', 'Beauty and the Beast'];
let series = [9,8,7,6,5,15,89,27,36];



//series.sort();
//library.sort();


//reverse () - reverse order of each element inside an array
series.reverse();



console.log(series);
console.log(library);


//2. Accessors

//indexof() - find/identify the index number of a given element
// in case of duplicates - will return the first instance of the value

let countries = ['US','PH','CAN','SG','CAN'];
let indexCount = countries.indexOf('CAN');
console.log('Located at Index: '+indexCount);

//lastIndexOf() - last/final instance of the element
let lastFound = countries.lastIndexOf('CAN');
console.log('Element last fund found at:' + lastFound);

//3. Iterators

bootcampTasks.forEach( function(task){
	// display individually inside console
	console.log(task);
})


// map()
let numbers = [1,2,3,4,5,6,7,8,9]

let words = ['Ball','Ball','Ball','Ball','Candy']

// numbers.map( function(num){
// 	console.log(num * num);
// })


//every() - check if lss elements pass a certain condition
let isPassed = words.every( function(word) {
	// better to use Return
	return (word <= 'Ball');
})
console.log(isPassed);


let allValid = numbers.every( function(num){
	return (num <= 3);
})

console.log(allValid);


// some() - atleast 1 should pass the condition

let isGreaterThan7 = numbers.some( function(num){
	return (num >= 7);
})
console.log('Atleast 1 element passed? '+ isGreaterThan7);


// filter() - will create a new array with values matching a given condition
let money = [1,5,10,20,50,100];
let paperMoney = money.filter( function(pera) {
	return (pera >= 20);
})
console.log(money);
console.log(paperMoney);


// reduce() - assess the element from left-to-right, returns single value

let outcomeNgReduce = money.reduce( function (initialElement, nextElement) {
	return (initialElement + nextElement);
})
console.log(outcomeNgReduce);


let crypto = ['BTC', 'ETH', 'DOGE', 'SLP', 'BNB', .00001, 42]
let outcome = crypto.reduce( function(left, right){
	return left + right;
})
console.log(outcome);